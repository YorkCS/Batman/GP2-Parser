module Processor where

import ErrM
import Common
import Control.Monad
import qualified Nodes
import qualified AbsRule
import qualified Rewriting
import PrintRule (printTree)
import Data.List (elemIndex, genericLength, sort)


convertRawRule :: RawRule a -> Err Rule
convertRawRule (RawRule n a l r i w) =
  let
    s = printTree n ++ "(" ++ (unwords $ lines $ printTree a) ++ ")"
  in do
    i' <- convertInterList i
    (ks, l') <- convertGraph i' l
    (_, r') <- convertGraph i' r
    w' <- convertWhere ks w
    return (Rule s l' r' (genericLength i') w')

convertInterList :: AbsRule.Interface a -> Err [NodeKey]
convertInterList (AbsRule.Itr _ (AbsRule.ILs _ i)) =
  let
    is = sort $ fmap convertNodeId i  -- convert and sort list
  in do
    _ <- foldM_ f Nothing is  -- check list for duplicate nodes
    return is                 -- return list if no duplicates
  where
    f :: Maybe NodeKey -> NodeKey -> Err (Maybe NodeKey)
    f (Just a) b = if a == b then Bad ("Duplicate interface node " ++ b) else Ok (Just b)
    f _ b = Ok (Just b)

convertNodeId :: AbsRule.NID a -> NodeKey
convertNodeId (AbsRule.NdS _ (AbsRule.Ident id)) = id
convertNodeId (AbsRule.NdI _ id) = show id

convertGraph :: [NodeKey] -> AbsRule.Graph a -> Err ([NodeKey], Graph)
convertGraph is (AbsRule.Gr _ (AbsRule.Nodes _ ns) (AbsRule.Edges _ es)) =
  do
    ns' <- mapM convertNode ns >>= foldM f Nodes.empty
    (keys, nodes) <- sortNodes is ns' >>= return . unzip
    edges <- mapM (convertEdge keys) es
    return (keys, Graph nodes edges)
    where
      f :: [(NodeKey, Node)] -> (NodeKey, Node) -> Err [(NodeKey, Node)]
      f ls (k, v) = Nodes.insert k v ls

convertNode :: AbsRule.Node a -> Err (NodeKey, Node)
convertNode (AbsRule.Nod _ n d) =
  makeNode (convertNodeId n) False d
convertNode (AbsRule.NodR _ n d) =
  makeNode (convertNodeId n) True d

makeNode :: NodeKey -> Root -> AbsRule.NodeData a -> Err (NodeKey, Node)
makeNode k r d = do
  (l, c) <- processNodeData d
  return (k, Node l c r)

processNodeData :: AbsRule.NodeData a -> Err (Maybe Label, Maybe Colour)
processNodeData (AbsRule.NDLab _ l) = do
  l' <- processLabel l
  return (l', Nothing)
processNodeData (AbsRule.NDLabC _ l c) = do
  l' <- processLabel l
  c' <- processNodeColor c
  return (l', Just c')

processLabel :: AbsRule.Label a -> Err (Maybe Label)
processLabel (AbsRule.LLab _ l) =
  let
    l' = Rewriting.processListExp [] l
  in case l' of
    Bad _ -> fail "Labels cannot reference nodes"
    Ok a -> return (Just (printTree a))
processLabel (AbsRule.ELab _ _) =
  return Nothing

processNodeColor :: AbsRule.Color -> Err Colour
processNodeColor (AbsRule.Color "red") = return Red
processNodeColor (AbsRule.Color "green") = return Green
processNodeColor (AbsRule.Color "blue") = return Blue
processNodeColor (AbsRule.Color "grey") = return Marked
processNodeColor (AbsRule.Color "any") = return Any
processNodeColor (AbsRule.Color color) = fail $ "Invalid node mark " ++ color

sortNodes :: [NodeKey] -> [(NodeKey, Node)] -> Err [(NodeKey, Node)]
sortNodes is ns = 
  do
    (as, bs) <- foldM f (Nodes.empty, ns) is
    return $ as ++ bs
  where
    f :: ([(NodeKey, Node)], [(NodeKey, Node)]) -> NodeKey -> Err ([(NodeKey, Node)], [(NodeKey, Node)])
    f (as, bs) k =
      do
        (n, bs') <- Nodes.pluck k bs
        as' <- Nodes.insert k n as
        return (as', bs')

convertEdge :: [NodeKey] -> AbsRule.Edge a -> Err Edge
convertEdge ks (AbsRule.Edg _ _ s t d) =
  makeEdge ks False s t d
convertEdge ks (AbsRule.EdgB _ _ s t d) =
  makeEdge ks True s t d

makeEdge :: [NodeKey] -> Bidirectional -> AbsRule.NID a -> AbsRule.NID a -> AbsRule.EdgeData a -> Err Edge
makeEdge ks b s t d = do
  (l, c) <- processEdgeData d
  s' <- processNode ks s
  t' <- processNode ks t
  return (Edge s' t' l c b)

processEdgeData :: AbsRule.EdgeData a -> Err (Maybe Label, Maybe Colour)
processEdgeData (AbsRule.EDLab _ l) = do
  l' <- processLabel l
  return (l', Nothing)
processEdgeData (AbsRule.EDLabC _ l c) = do
  l' <- processLabel l
  c' <- processEdgeColor c
  return (l', Just c')

processEdgeColor :: AbsRule.Color -> Err Colour
processEdgeColor (AbsRule.Color "red") = return Red
processEdgeColor (AbsRule.Color "green") = return Green
processEdgeColor (AbsRule.Color "blue") = return Blue
processEdgeColor (AbsRule.Color "dashed") = return Marked
processEdgeColor (AbsRule.Color "any") = return Any
processEdgeColor (AbsRule.Color color) = fail $ "Invalid edge mark " ++ color

processNode :: [NodeKey] -> AbsRule.NID a -> Err NodeId
processNode ks n =
  let
    id = convertNodeId n
    index = elemIndex id ks
  in case index of
    Just i -> return $ toInteger i
    Nothing -> fail $ "Node " ++ id ++ " not found but used by edge"

convertWhere :: [NodeKey] -> Maybe (AbsRule.WhereCond a) -> Err Where
convertWhere _ Nothing = Ok Nothing
convertWhere ks (Just (AbsRule.Wher _ c)) = do
  c' <- Rewriting.processCondition ks c
  return (Just (printTree c'))
