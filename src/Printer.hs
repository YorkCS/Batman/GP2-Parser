module Printer where

import Common
import Data.Maybe (catMaybes)

type Standalone = Bool

renderGraph :: Standalone -> Interface -> Graph -> String
renderGraph s i (Graph ns es) =
  let
    ns' = fmap (renderNode i) $ zip [0..] ns
    es' = fmap renderEdge es
  in
    f $ ns' ++ es'
  where
    f x =
      let
        pad = if s then "0.18" else "0.06"
        init =
          [ "forcelabels=true"
          , "graph [pad=\"" ++ pad ++ "\" nodesep=\"0.8\" ranksep=\"1.1\"]"
          , "node [height=0.5 fixedsize=true]"
          ]
        ls = fmap (\y -> "  " ++ y) $ init ++ x
      in
        "digraph {\n" ++ unlines ls ++ "}"

renderNode :: Interface -> (NodeId, Node) -> String
renderNode i (id, (Node l c r)) =
  let
    i' = renderNodeId i id
    l' = Just (renderNodeLabel l)
    c' = fmap renderNodeColour c
    r' = Just (renderNodeRooted r)
  in
    show id ++ wrap (catMaybes [i', l', c', r'])

renderNodeId :: Interface -> NodeId -> Maybe String
renderNodeId i id =
  if id < i then Just ("xlabel=\"" ++ show id ++ "\"") else Nothing

renderNodeLabel :: Maybe Label -> String
renderNodeLabel (Just l) = "label=\"" ++ escape l ++ "\""
renderNodeLabel Nothing = "label=\" \""

renderNodeColour :: Colour -> String
renderNodeColour Red    = "style=filled fillcolor=red"
renderNodeColour Green  = "style=filled fillcolor=green"
renderNodeColour Blue   = "style=filled fillcolor=blue"
renderNodeColour Marked = "style=filled fillcolor=grey"
renderNodeColour Any    = "style=filled fillcolor=pink"

renderNodeRooted :: Root -> String
renderNodeRooted True  = "shape=doublecircle"
renderNodeRooted False = "shape=circle"

renderEdge :: Edge -> String
renderEdge (Edge s t l c b) =
  let
    l' = fmap renderEdgeLabel l
    c' = fmap renderEdgeColour c
    b' = renderEdgeDirection b
  in
    show s ++ " -> " ++ show t ++ wrap (catMaybes [l', c', b'])

renderEdgeLabel :: Label -> String
renderEdgeLabel l = "label=\" " ++ escape l ++ " \""

renderEdgeColour :: Colour -> String
renderEdgeColour Red    = "color=red"
renderEdgeColour Green  = "color=green"
renderEdgeColour Blue   = "color=blue"
renderEdgeColour Marked = "style=dashed"
renderEdgeColour Any    = "color=pink"

renderEdgeDirection :: Bidirectional -> Maybe String
renderEdgeDirection True = Just "dir=none"
renderEdgeDirection False = Nothing

escape :: String -> String
escape s = s >>= f
  where
    f '"' = "\\\""
    f c = [c]

wrap :: [String] -> String
wrap [] = ""
wrap s = " [" ++ unwords s ++ "]"
