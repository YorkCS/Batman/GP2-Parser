module Rewriting (processCondition, processLabel, processListExp) where

import ErrM
import Common
import Control.Monad
import qualified AbsRule
import Data.List (elemIndex)

-- rewrite any and all node ids
processCondition :: [NodeKey] -> AbsRule.Condition a -> Err (AbsRule.Condition a)
-- work on the lhs and rhs recursively
processCondition ks (AbsRule.CondOr a x y) = do
  x' <- processCondition ks x
  y' <- processCondition ks y
  return (AbsRule.CondOr a x' y')
-- work on the lhs and rhs recursively
processCondition ks (AbsRule.CondAnd a x y) = do
  x' <- processCondition ks x
  y' <- processCondition ks y
  return (AbsRule.CondAnd a x' y')
-- work on the inner exp recursively
processCondition ks (AbsRule.CondNot a x) = do
  x' <- processCondition ks x
  return (AbsRule.CondNot a x')
-- convert the node ids from both sides
processCondition ks (AbsRule.CondEdg a x y) = do
  x' <- renameNode ks x
  y' <- renameNode ks y
  return (AbsRule.CondEdg a x' y')
-- convert the node ids of both nodes and in the label
processCondition ks (AbsRule.CondEdgL a x y z) = do
  x' <- renameNode ks x
  y' <- renameNode ks y
  z' <- processLabel ks z
  return (AbsRule.CondEdgL a x' y' z')
-- work on the lhs and rhs recursively
processCondition ks (AbsRule.CondEq a x op y) = do
  x' <- processListExp ks x
  y' <- processListExp ks y
  return (AbsRule.CondEq a x' op y')
-- work on the lhs and rhs recursively
processCondition ks (AbsRule.CondOrd a x op y) = do
  x' <- processAtomExp ks x
  y' <- processAtomExp ks y
  return (AbsRule.CondOrd a x' op y')

processLabel :: [NodeKey] -> AbsRule.Label a -> Err (AbsRule.Label a)
-- process the inner list exp
processLabel ks (AbsRule.LLab a x) = do
  x' <- processListExp ks x
  return (AbsRule.LLab a x')
-- otherwise, do nothing and pass thru
processLabel _ a = return a

processListExp :: [NodeKey] -> AbsRule.ListExp a -> Err (AbsRule.ListExp a)
-- process each list item recursively
processListExp ks (AbsRule.LsExp a xs) = do
  xs' <- mapM (processListVal ks) xs
  return (AbsRule.LsExp a xs')

processListVal :: [NodeKey] -> AbsRule.ListVal a -> Err (AbsRule.ListVal a)
-- work on the atom expression recursively
processListVal ks (AbsRule.LsVal a x) = do
  x' <- processAtomExp ks x
  return (AbsRule.LsVal a x')

processAtomExp :: [NodeKey] -> AbsRule.AtomExp a -> Err (AbsRule.AtomExp a)
-- work on the lhs and rhs recursively
processAtomExp ks (AbsRule.AtConc a x y) = do
  x' <- processAtomExp ks x
  y' <- processAtomExp ks y
  return (AbsRule.AtConc a x' y')
-- work on the lhs and rhs recursively
processAtomExp ks (AbsRule.AtArith a x op y) = do
  x' <- processAtomExp ks x
  y' <- processAtomExp ks y
  return (AbsRule.AtArith a x' op y')
-- work on the inner exp recursively
processAtomExp ks (AbsRule.AtNeg a x) = do
  x' <- processAtomExp ks x
  return (AbsRule.AtNeg a x')
-- convert the node id in the exp
processAtomExp ks (AbsRule.AtIndeg a x) = do
  x' <- renameNode ks x
  return (AbsRule.AtIndeg a x')
-- convert the node id in the exp
processAtomExp ks (AbsRule.AtOutdeg a x) = do
  x' <- renameNode ks x
  return (AbsRule.AtOutdeg a x')
-- otherwise, do nothing and pass thru
processAtomExp _ a = return a

renameNode :: [NodeKey] -> AbsRule.NID a -> Err (AbsRule.NID a)
renameNode ks nd@(AbsRule.NdS a _) = do
  id <- processNode ks nd
  return (AbsRule.NdI a id)
renameNode ks nd@(AbsRule.NdI a _) = do
  id <- processNode ks nd
  return (AbsRule.NdI a id)

processNode :: [NodeKey] -> AbsRule.NID a -> Err NodeId
processNode ks n =
  let
    id = g n
    index = elemIndex id ks
  in case index of
    Just i -> return $ toInteger i
    Nothing -> fail $ "Node " ++ id ++ " not found but used by condition or label"
  where
    g :: AbsRule.NID a -> NodeKey
    g (AbsRule.NdS _ (AbsRule.Ident id)) = id
    g (AbsRule.NdI _ id) = show id

convertNodeId :: AbsRule.NID a -> NodeKey
convertNodeId (AbsRule.NdS _ (AbsRule.Ident id)) = id
convertNodeId (AbsRule.NdI _ id) = show id
