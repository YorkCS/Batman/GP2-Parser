module Nodes where

import ErrM
import Data.List
import Common (NodeKey)

type Nodes a = [(NodeKey, a)]

empty :: Nodes a
empty = []

pluck :: NodeKey -> Nodes a -> Err (a, Nodes a)
pluck n ns = 
  let
    (matches, remaining) = partition (\(k, _) -> k == n) ns
  in
    case matches of
      [] -> Bad ("Undefined node " ++ n ++ " used in interface")
      [(_, v)] -> Ok (v, remaining)
      -- other cases should be impossible

insert :: NodeKey -> a -> Nodes a -> Err (Nodes a)
insert n v ns =
  let
    match = find (\(k, _) -> k == n) ns
  in
    case match of
      Just _ -> Bad ("Double occurrence of node " ++ n)
      Nothing -> Ok (ns ++ [(n,v)])
