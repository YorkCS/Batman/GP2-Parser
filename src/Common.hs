module Common where

import qualified AbsRule

type Root = Bool
type Label = String
type NodeId = Integer
type NodeKey = String
type Signature = String
type Interface = Integer
type Bidirectional = Bool
type Where = Maybe String

data RawRule a = RawRule (AbsRule.RuleName a) (AbsRule.ArgList a) (AbsRule.Graph a) (AbsRule.Graph a) (AbsRule.Interface a) (Maybe (AbsRule.WhereCond a))
  deriving (Show, Eq)

data Rule = Rule Signature Graph Graph Interface Where
  deriving (Show, Eq)

data Graph = Graph [Node] [Edge]
  deriving (Show, Eq)

data Node = Node (Maybe Label) (Maybe Colour) Root
  deriving (Show, Eq)

data Edge = Edge NodeId NodeId (Maybe Label) (Maybe Colour) Bidirectional
  deriving (Show, Eq)

data Colour = Red | Green | Blue | Marked | Any
  deriving (Show, Eq)
